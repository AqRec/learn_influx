[1] Grafana:
``` bash
docker run -d --name=grafana -p 3000:3000 grafana/grafana

```

[2] InfluxDB:
``` bash
mkdir -p db/influxdb/data
mkdir -p db/influxdb/wal
```
vim configs/influxdb.conf
```
[data]
  dir = '/home/flatland/practice/spidernet/db/influxdb/data'
  wal-dir = '/home/flatland/practice/spidernet/db/influxdb/wal'

[admin]
  # the host and port for admin page
  bind-address='127.0.0.1:8083'

[http]
  # the host and port for api
  bind-address = ':8086'

```

vim configs/influxdb/influx_settings.conf <br>
this conf is for python
```
[db]
# mongodb db and collection to be mornitored
db_collection_dict = {
    'data_db': 'data_collection'
}

[time]
interval = 15
```

``` bash
sudo docker run --rm --name=influx -p 8086:8086 -v ls $(pwd)/db/influxdb/data:/var/lib/influxdbsudo docker run --rm -d --name=influx -p 8086:8086 -v $(pwd)/db/influxdb/data:/var/lib/influxdb -v $(pwd)/configs/influxdb.conf:/etc/influxdb/influxdb.conf:ro influxdb:1.7.9 -config /etc/influxdb/influxdb.conf
```
