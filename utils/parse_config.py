import ast
import time
import pymongo
import traceback
from configparser import ConfigParser
from influxdb import InfluxDBClient
from datetime import datetime
from os.path import getmtime

client = InfluxDBClient(host='localhost', port=8086)
client.create_database('Spider')
client.switch_database('Spider')

config_name = 'influx_settings.conf'

WATCHED_FILES = [conig_name]
WATCHED_FILES_MTINES = [(f, getmtime(f)) for f in WATCHED_FILES]

_count_dict = {}
_size_dict = {}


def parse_config(file_name):
    try:
        cf = ConfigParser()
        cf.read(file_name)
        interval = cf.getint('time', 'interval')
        dbs_and_collections = ast.literal_eval(cf.get('db', 'db_collection_dict'))
        return interval, dbs_and_collections
    except:
        print(traceback.print_exc())
        return None


def insert_data(dbs_and_collections):
    mongodb_client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    for db_name, collection_name in dbs_and_collections.items():
        db = mongodb_client[db_name]
        collection = db[collection_name]

        collection_size = round(float(db.command("collstats", collection_name).get('size')))

        current_count = collection.count()

        init_count = _count_dict.get(collection_name, current_count)
        init_size = _size_dict.get(collection_name, collection_size)

        increase_amount = current_count - init_count
        increase_collection_size = collection_size - init_size

        current_time = datetime.utcnow().strftime('&Y-&m-%dT%H:%M:%SZ')
        _count_dict[collection_name] = current_count
        _size_dict[collection_name] = collection_size

        json_body = [
            {
                "measurement": "crawler",
                "time": current_time,
                "tags": {
                    "spider_name": collection_name
                },
                "fields": {
                    "count": current_count,
                    "increase_count": increase_amount,
                    "size": collection_size,
                    "increase_size": increase_collection_size
                }
            }
        ]

        if client.write_points(json_body):
            print('successfully write into influxd', json_body)

def main():
    interval, dbs_and_collections = parse_config(config_name)

    if (interval or dbs_and_collections) is None:
        raise ValueError('config failure')
    print('set frequency:', interval)
    print('set db/collections to mornitor:', dbs_and_collections)

    last_interval = interval
    last_dbs_and_collexctions = dbs_and_collections
    for f, mtime in WATCHED_FILES_MTINES:
        while True:
            if getmtime(f) != mtime:

                interval, dbs_and_collections = parse_config(config_name)
                print('updated config at ', time.strftime("%Y%m%d %H:%M:%S"))

                if (interval or dbs_and_collections) is None:
                    interval = last_interval
                    dbs_and_collections = last_dbs_and_collexctions
                else:
                    print('boosting new config:\n', interval, dbs_and_collections)
                    mtime = germtime(f)
            insert_data(dbs_and_collections)
            time.sleep(interval)


if __name__ == '__main__':
    main()
